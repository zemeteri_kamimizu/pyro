
class CommandProcessor:
    def __init__(self):
        self.__subs = {}
    def subscribe(self, cmd, sub):
        if not cmd in self.__subs.keys():
            self.__subs[cmd] = []
        self.__subs[cmd].append(sub)
    def unsubscribe(self, cmd, sub):
        if not cmd in self._subs.keys():
            return
        self.__subs[cmd].remove(sub)
    def exec(self, cmd):
        strcmd = cmd.decode()
        if strcmd == 'SHORT':
            print('CommandProcessor::exec(): executing short press ({})'.format(strcmd))
        elif strcmd == 'LONG':
            print('CommandProcessor::exec(): executing long press ({})'.format(strcmd))
        elif strcmd == 'VERYLONG':
            print('CommandProcessor::exec(): executing very long press ({})'.format(strcmd))
        elif strcmd == 'VERYVERYLONG':
            print('CommandProcessor::exec(): executing very very long press ({})'.format(strcmd))
        else:
            print('CommandProcessor::exec(): error unknown command {}'.format(cmd))
            return
        if not strcmd in self.__subs.keys():
            return
        for s in self.__subs[strcmd]:
            s.update(strcmd)
