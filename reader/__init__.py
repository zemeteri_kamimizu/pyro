
class BufferedReader:
    def __init__(self, buf):
        self.__buf = buf
    def read(self):
        dat = self._impl(self.__buf.avail())
        if dat is None or len(dat) == 0:
            return
        self.__buf.put(dat)
