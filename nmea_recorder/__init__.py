import time
from subscriber import Subscriber

class NmeaRecorder(Subscriber):
    class NmeaRecordingSubscriber(Subscriber):
        def __init__(self, cfg):
            self.__outdir = cfg['dir']
            ld = time.localtime()
            self.__name = '{}/{:04}-{:02}-{:02}_{:02}-{:02}-{:02}_route.nmea.log'.format(self.__outdir, ld.tm_year, ld.tm_mon, ld.tm_mday, ld.tm_hour, ld.tm_min, ld.tm_sec)
            self.__file = open(self.__name, 'w')
        def _impl(self, dat):
            self.__file.write(dat)
            self.__file.write('\r\n')
        def name(self):
            return self.__name
        def close(self):
            self.__file.close()

    STATE_IDLE = 0
    STATE_RECORDING = 1
    def __init__(self, cfg, nmeapub):
        self.__cfg = cfg
        self.__state = NmeaRecorder.STATE_IDLE
        self.__state_mtx = [self.__start_recording, self.__stop_recording]
        self.__writer = None
        self.__subs = []
        self.__nmea_pub = nmeapub
    def subscribe(self, sub):
        self.__subs.append(sub)
    def unsubscribe(self, sub):
        self.__subs.remove(sub)
    def _impl(self, cmd):
        # if we got VERYLONG command (very long key press) - change state (record or stop record)
        if cmd == 'VERYLONG':
            self.__state = self.__state_mtx[self.__state]()
            self.__publish()
        # if we got 
        elif cmd == 'LONG':
            print('*** SAVE A CHECKPOINT: {}'.format('nmea data: 12345678'))
    def __publish(self):
        for s in self.__subs:
            s.update(True if self.__state == NmeaRecorder.STATE_RECORDING else False)
    def __start_recording(self):
        print('Start recording of the route')
        self.__writer = NmeaRecorder.NmeaRecordingSubscriber(self.__cfg)
        self.__nmea_pub.subscribe(self.__writer)
        return NmeaRecorder.STATE_RECORDING # state to be set
    def __stop_recording(self):
        print('Stop recording of the route')
        self.__nmea_pub.unsubscribe(self.__writer)
        self.__writer.close()
        self.__writer = None
        return NmeaRecorder.STATE_IDLE # state to be set
