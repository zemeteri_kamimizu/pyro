from singleton import SingletonMeta
import select

import sys

class Events:
    ERR  = select.EPOLLERR
    ET   = select.EPOLLET
    XCLS = select.EPOLLEXCLUSIVE
    HUP  = select.EPOLLHUP
    IN   = select.EPOLLIN
    MSG  = select.EPOLLMSG
    ONE  = select.EPOLLONESHOT
    OUT  = select.EPOLLOUT
    PRI  = select.EPOLLPRI
    RDB  = select.EPOLLRDBAND
    RDH  = select.EPOLLRDHUP
    RDN  = select.EPOLLRDNORM
    WRB  = select.EPOLLWRBAND
    WRN  = select.EPOLLWRNORM
    CLO  = select.EPOLL_CLOEXEC

class EventLoopGuts(metaclass=SingletonMeta):
    def __init__(self):
        self._evloop = select.epoll()
        self._handlers = dict()
        self._mgmt = {'working':1}

class EventLoop(object):
    def __init__(self):
        self._guts = EventLoopGuts()
    def run(self):
        while self._guts._mgmt['working'] != 0:
            try:
                # poll events
                evts = self._guts._evloop.poll(timeout=1, maxevents=-1)
                if len(evts) == 0:
                    continue
                # find handlers and enqueue them accordingly to their priority
                for e in evts:
                    fd = e[0]
                    ev = e[1]
                    self._guts._handlers[fd].handle(ev)
            except: # first for InterruptedSystemCall exception
                print('EventLoop:run(): {}'.format(sys.exc_info()))
                # pass
    def halt(self):
        self._guts._mgmt['working'] = 0
    def release(self):
        em = EventManager()
        l = list(self._guts._handlers.values())
        for h in l:
            em.fire(h)
        self._guts._evloop.close()

class EventManager(object):
    def __init__(self):
        self._guts = EventLoopGuts()
    def reg_in(self, handler):
        if not handler.fd().fileno() in self._guts._handlers.keys():
            self._guts._handlers[handler.fd().fileno()] = handler
        self._guts._evloop.register(handler.fd(), select.EPOLLIN)
    def reg_out(self, handler):
        if not handler.fd().fileno() in self._guts._handlers.keys():
            self._guts._handlers[handler.fd().fileno()] = handler
        self._guts._evloop.register(handler.fd(), select.EPOLLOUT)
    def unreg(self, handler):
        self._guts._evloop.unregister(handler.fd())
    def fire(self, handler):
        try:
            self.unreg(handler)
        except:
            pass
        if handler.fd().fileno() in self._guts._handlers.keys():
            self._guts._handlers.pop(handler.fd().fileno())
        handler.leave()
