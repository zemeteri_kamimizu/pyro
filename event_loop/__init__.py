from . import event_loop

import os

class EventHandler(object):
    def __init__(self, fd):
        self._fd = fd
    def _impl(self, evt):
        pass
    def _fini(self):
        pass
    def fd(self):
        return self._fd
    def handle(self, evt):
        self._impl(evt)
    def leave(self):
        try:
            self._fini()
        except:
            pass
        try:
            os.close(self._fd.fileno())
        except:
            pass

events = event_loop.Events()

def evloop():
    return event_loop.EventLoop()

def evmgr():
    return event_loop.EventManager()

def halt():
    loop = event_loop.EventLoop()
    loop.halt()
