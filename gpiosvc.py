import RPi.GPIO as GPIO
import json

from tools.gettime import timenow
from tools.daemon import daemon
from tools.cmdargs import cmd_line_args
from uxdgramrxtx import uxdgram_transmitter

"""
This is an implementation for RapsberryPi
boards. There is one more known module for
OrangePi boards - OrangePi.GPIO, which has
some differences:
  1. Name - import OPi.GPIO as GPIO
  2. Usage - there is no wait_for_edge()
    function, so use time.sleep(sleep_msec)
"""

###

class GpioEventCycle:
    def __init__(self, pincfg, notif):
        self.__pincfg = pincfg
        self.__press_time_ns = 0
        self.__release_time_ns = 0
        self.__notif = notif
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.__pincfg['pin'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    def run(self):
        # in nanoseconds
        short = int(self.__pincfg['short'] * 1000000000)
        long = int(self.__pincfg['long'] * 1000000000)
        longlong = int(self.__pincfg['verylong'] * 1000000000)
        while work[0]:
            chnl = GPIO.wait_for_edge(self.__pincfg['pin'], GPIO.BOTH, timeout=1)

            if chnl is None:
                continue

            act = GPIO.input(chnl)

            # if key is pressed
            if act:
                self.__press_time_ns = timenow()
                continue
            # if key is released
            self.__release_time_ns = timenow()
            dt = self.__release_time_ns - self.__press_time_ns
            dat = None
            if dt <= short:
                dat = b'SHORT'
                print("Got SHORT_TAP")
            elif dt > short and dt <= long:
                dat = b'LONG'
                print("Got LONG_TAP")
            elif dt > long and dt <= longlong:
                dat = b'VERYLONG'
                print("Got VERY_LONG_TAP")
            else:
                dat = b'VERYVERYLONG'
                print("Got VERY_VERY_LONG_TAP")

            try:
                if not dat is None:
                    self.__notif.send(dat)
            except:
                pass

    def finish(self):
        GPIO.cleanup()

###

prog_conf = 'settings.json'
pid_file = 'gpiod.pid'
stdout = None
stderr = None
work = [1]

def sighnd(sig, frame):
    print('Signal received')
    work[0] = 0

if __name__ == '__main__':
    args = cmd_line_args()

    if args.conf_file():
        prog_conf = args.conf_file()

    if args.log_stdout():
        stdout = args.log_stdout()
    if args.log_stderr():
        stderr = args.log_stderr()
    if args.pid_file():
        pid_file = args.pid_file()

    # read configuration file
    cfg = None
    try:
        with open(prog_conf, 'r') as f:
            cfg = json.load(f)
    except:
        print('Can not read configuration file {}'.format(prog_conf))
        exit(1)

    daemon(sighnd, pid_file, stdout, stderr, args.is_daemon())

    try:
        tsmt = uxdgram_transmitter(cfg['gpiod']['notif']['peer'])
    except Exception as e:
        print("Can not create datagram transmitter: {}".format(e))
        exit(2)

    gpio_cycle = GpioEventCycle(cfg['gpiod']['input'], tsmt)

    gpio_cycle.run()

    gpio_cycle.finish()
