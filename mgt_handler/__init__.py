from event_loop import EventHandler, evmgr
from uxdgramrxtx import uxdgram_receiver

class Management:
    class ManagementHandler(EventHandler):
        def __init__(self, fd, cfg, cmdproc):
            super(Management.ManagementHandler, self).__init__(fd)
            self.__cfg = cfg
            self.__cmdproc = cmdproc
        def _impl(self, evt):
            cmd, peer = self._fd.recvfrom(self.__cfg['bufsz'])
            print('Incoming command from {}'.format(peer))
            self.__cmdproc.exec(cmd)
        def _fini(self):
            pass
    def __init__(self, cfg, cmdproc):
        self.__rcv = uxdgram_receiver(cfg['bufsz'], cfg['bind'])
        self.__hnd = Management.ManagementHandler(self.__rcv.sock(), cfg, cmdproc)
    def start(self):
        em = evmgr()
        em.reg_in(self.__hnd)
    def stop(self):
        em = evmgr()
        em.fire(self.__hnd)
        self.__rcv.close()
