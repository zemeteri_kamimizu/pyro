from task import Task

class NmeaPublisherTask(Task):
    def __init__(self, buf):
        self.__buf = buf
        self.__subs = list()
    def _impl(self):
        dat = self.__buf.get()
        while not dat is None:
            for s in self.__subs:
                s.update(dat)
            dat = self.__buf.get()
    def subscribe(self, sub):
        self.__subs.append(sub)
    def unsubscribe(self, sub):
        self.__subs.remove(sub)