from task import Task

class UartTask(Task):
    def __init__(self, rdr):
        self.__reader = rdr
    def _impl(self):
        self.__reader.read()
