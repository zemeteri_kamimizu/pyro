import RPi.GPIO as GPIO
# import OPi.GPIO as GPIO
from subscriber import Subscriber

class GpioStateIndicator(Subscriber):
    def __init__(self, cfg):
        self.__ready_pin = cfg['ready']['pin']
        self.__recording_pin = cfg['record']['pin']
        self.__gpio_init = False
        try:
            # for OPi
            # self.__ready_pin = GPIO.PC+4
            # self.__recording_pin = GPIO.PA+21
            # GPIO.setboard(GPIO.PCPCPLUS)
            # GPIO.setmode(GPIO.SOC)
            # for RPi
            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            GPIO.setup(self.__ready_pin, GPIO.OUT)
            GPIO.setup(self.__recording_pin, GPIO.OUT)
            GPIO.output(self.__ready_pin, 0)
            GPIO.output(self.__recording_pin, 0)
        except Exception as e:
            print('GpioStateIndicator.__init__(): exception {}'.format(e))
        else:
            self.__gpio_init = True
    def _impl(self, state):
        if self.__gpio_init is False:
            return
        try:
            GPIO.output(self.__recording_pin, 1 if state is True else 0)
        except Exception as e:
            print('GpioStateIndicator._impl(): {}'.format(e))
    def on(self):
        if self.__gpio_init is False:
            return
        try:
            GPIO.output(self.__ready_pin, 1)
        except Exception as e:
            print('GpioStateIndicator.on(): exception {}'.format(e))
    def off(self):
        if self.__gpio_init is False:
            return
        try:
            GPIO.output(self.__ready_pin, 0)
        except Exception as e:
            print('GpioStateIndicator.on(): exception {}'.format(e))
    def close(self):
        GPIO.cleanup()