
class Buffer:
    def __init__(self):
        pass
    def put(self, dat):
        pass
    def get(self):
        pass
    def avail(self):
        pass

class LineBuffer(Buffer):
    def __init__(self, chunksz):
        self.__buf = bytes()
        self.__chs = chunksz
        self.__lst = []
    def put(self, dat):
        tbuf = self.__buf + dat
        for l in tbuf.decode().split('\r\n'):
            if len(l) == 0:
                self.__buf = bytes()
                break
            self.__lst.append(l)
            self.__buf = l.encode() # save 'the tail' if the last string in the list is not empty
    def get(self):
        return self.__lst.pop(0) if len(self.__lst) else None
    def avail(self):
        return self.__chs
