import socket
import os

class uxdgram_transmitter(object):
    def __init__(self, peer):
        self._sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        self._peer = peer
    def send(self, data):
        self._sock.sendto(data, self._peer)
    def sock(self):
        return self._sock
    def fd(self):
        return self._sock.fileno()
    def close(self):
        try:
            self._sock.close()
        except:
            pass

class uxdgram_receiver(object):
    def __init__(self, maxlen, bind):
        self._maxlen = maxlen
        self._bind = bind
        self._sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        self._sock.bind(bind)
    def recv(self):
        data, self._peer = self._sock.recvfrom(self._maxlen)
        return data
    def sock(self):
        return self._sock
    def fd(self):
        return self._sock.fileno()
    def close(self):
        try:
            self._sock.close()
        except:
            pass
        finally:
            os.unlink(self._bind)
