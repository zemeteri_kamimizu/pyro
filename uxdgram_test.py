import socket

if __name__ == '__main__':
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    sock.bind('main.usock')
    while True:
        dat, peer = sock.recvfrom(64)
        print('Received {} from {}'.format(dat.decode(), str(peer)))
