from event_loop import evloop
from task import TaskHandler

from tasks.uart_task import UartTask
from tasks.nmea_publisher_task import NmeaPublisherTask

from tools.daemon import daemon
from tools.cmdargs import cmd_line_args

from buffer import LineBuffer
from uart import UART_Reader
from timer_handler import TimerHandler
# from tcp_server.server import TcpServer

from subscriber import Subscriber
from mgt_handler import Management
from cmdproc import CommandProcessor

from nmea_recorder import NmeaRecorder
from gpio_state_indicator import GpioStateIndicator

import json

prog_conf = 'settings.json'
pid_file = '/var/run/nmead.pid'
stdout = None
stderr = None

main_event_loop = None

class PrintingSubscriber(Subscriber):
    def __init__(self):
        pass
    def _impl(self, dat):
        print('Got subscription update: {}'.format(dat))

def sighnd(sig, frame):
    main_event_loop.halt()

if __name__ == '__main__':
    args = cmd_line_args()

    if args.conf_file():
        prog_conf = args.conf_file()

    if args.log_stdout():
        stdout = args.log_stdout()
    if args.log_stderr():
        stderr = args.log_stderr()
    if args.pid_file():
        pid_file = args.pid_file()

    # read configuration file
    cfg = None
    try:
        with open(prog_conf, 'r') as f:
            cfg = json.load(f)
    except:
        print('Can not read configuration file {}'.format(prog_conf))
        exit(1)

    # daemonize (or not)
    daemon(sighnd, pid_file, stdout, stderr, args.is_daemon())

    # create main event loop
    main_event_loop = evloop()

    # create buffer
    buf = LineBuffer(cfg['main']['buf']['size'])

    # create UART task handler
    uart_reader = UART_Reader(buf, cfg['main']['uart'])

    # create task handler & task
    uart_task = UartTask(uart_reader)

    # create publisher task
    nmea_pub_task = NmeaPublisherTask(buf)

    # create task handler and add tasks
    htask = TaskHandler()
    htask.add(uart_task)
    htask.add(nmea_pub_task)

    # create subscribers
    prnsub = PrintingSubscriber()

    # subscribe all subscribers
    nmea_pub_task.subscribe(prnsub)

    # create timer handler
    htime = TimerHandler(htask)

    # create tcp server
    # tcp = TcpServer(buf, cfg['main']['tcp'])
    # tcp.start()

    # NMEA data recorder
    rec = NmeaRecorder(cfg['main']['recorder'], nmea_pub_task)

    # Command publisher
    cmdpub = CommandProcessor()
    # cmdpub.subscribe('SHORT', None)
    cmdpub.subscribe('LONG', rec)
    cmdpub.subscribe('VERYLONG', rec)
    # cmdpub.subscribe('VERYVERYLONG', None)

    # State indicator
    ind = GpioStateIndicator(cfg['main']['indicator'])
    rec.subscribe(ind)

    # Management event handler object
    mgt = Management(cfg['main']['udp'], cmdpub)
    mgt.start()

    # light 'ready' LED
    ind.on()

    # main event loop run
    main_event_loop.run()

    ind.off()
    nmea_pub_task.unsubscribe(prnsub)
    rec.unsubscribe(ind)
    cmdpub.unsubscribe('LONG', rec)
    cmdpub.unsubscribe('VERYLONG', rec)

    # tcp.stop()
    mgt.stop()
    ind.close()
    exit(0)
