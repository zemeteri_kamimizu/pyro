# from task import Task
from reader import BufferedReader
from serial import Serial

class UART_Reader(BufferedReader):
    def __init__(self, buf, cfg):
        super(UART_Reader, self).__init__(buf)
        self.__cfg = cfg
        self.__uart = Serial(port=self.__cfg['dev'], baudrate=self.__cfg['baud'])
    def _impl(self, bsize):
        if self.__uart.in_waiting > 0:
            sz = bsize if bsize < self.__uart.in_waiting else self.__uart.in_waiting
            return self.__uart.read(sz)
        return None
