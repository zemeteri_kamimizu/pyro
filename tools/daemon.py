import os
import sys
import signal
import resource

def daemon(sighnd, pidfn, stdoutfn, stderrfn, daemonize=True):
    ppid = 0

    signal.signal(signal.SIGINT,  sighnd)
    signal.signal(signal.SIGTERM, sighnd)

    if not daemonize:
        return

    # do this stuff only if we were told to daemonize

    """
    Create first child
    """
    try:
        ppid = os.fork()
    except OSError as e:
        print("[{}] {}".format(e.errno, e.strerror))
        os._exit(1)

    if ppid != 0:
        print("Forked into child with PID {}".format(ppid))
        try:
            p,s = os.waitpid(ppid, 0)
            print('p={} s={:04x}'.format(p, s))
            signum = s & 0xff
            is_sig = True if signum else False
            status = (s & 0xff00) >> 8
            if status != 0:
                print('Child {}: signum={} is_sig={} status={} ({})'.format(p, signum, is_sig, status, s))
                os._exit(status)
        except OSError as e:
            print("Exception caught while 'waitpid()' syscall: {}".format(e))
            os._exit(1)
        os._exit(0)

    """
    Close all opened file descriptors and reopen them again
    """
    soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
    # some stupid nothing:
    if hard < soft:
        soft = hard
    for d in range(0, soft):
        try:
            os.close(d)
        except:
            pass

    try:
        sys.stdin = open("/dev/null", 'rt')
    except:
        os._exit(2)

    if stdoutfn is None:
        stdoutfn = '/dev/null'
    try:
        sys.stdout = open(stdoutfn, 'wt', buffering=1, newline='\n')
    except:
        os._exit(3)

    if stderrfn is None:
        stderrfn = '/dev/null'
    try:
        sys.stderr = open(stderrfn, 'wt', buffering=1, newline='\n')
    except:
        os._exit(4)

    """
    Let the first child be a session leader
    """
    os.setsid()

    """
    Create a child of first child
    """
    try:
        ppid = os.fork()
    except OSError as e:
        print("[{}] {}".format(e.errno, e.strerror))
        os._exit(5)

    """
    If we are parent - write PID to file and exit
    """
    if ppid != 0:
        print("Forked into child's child with PID {}".format(ppid))
        try:
            with open(pidfn, 'w') as f:
                f.write(str(ppid))
        except:
            os._exit(6)
        os._exit(0)

    return
