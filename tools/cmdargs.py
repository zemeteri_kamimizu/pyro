import argparse

class cmd_line_args(object):
    __impl = argparse.ArgumentParser()
    __args = None
    def __init__(self):
        self.__impl.add_argument('-d', '--daemon', default=False, action='store_true', help='run VM as daemon')
        self.__impl.add_argument('-c', '--conf', type=str, help='use configuration from file')
        self.__impl.add_argument('--stdout', default=None, type=str, help='set file for possible log')
        self.__impl.add_argument('--stderr', default=None, type=str, help='set file for error log')
        self.__impl.add_argument('--pidfile', default=None, type=str, help='write actual PID to specified file')
        self.__args = self.__impl.parse_args()

    def is_daemon(self):
        return self.__args.daemon

    def pid_file(self):
        return self.__args.pidfile

    def conf_file(self):
        return self.__args.conf

    def log_stdout(self):
        return self.__args.stdout

    def log_stderr(self):
        return self.__args.stderr
