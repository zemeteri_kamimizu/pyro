from event_loop import EventHandler, evmgr
import time
from linuxfd import timerfd

class TimerHandler:
    class th_ev_handler(EventHandler):
        def __init__(self, fd, task_handler):
            super(TimerHandler.th_ev_handler,self).__init__(fd)
            self.__th = task_handler
        def _impl(self, evt):
            self._fd.read()
            self.__th.run()

    def __init__(self, task_handler):
        self.__evmgr = evmgr()
        self.__tfd = timerfd(rtc=True, nonBlocking=True)
        self.__tfd.settime(1.0, 1.0)
        self.__hnd = TimerHandler.th_ev_handler(self.__tfd, task_handler)
        self.__evmgr.reg_in(self.__hnd)
