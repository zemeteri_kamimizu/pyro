import socket
from event_loop import EventHandler, events, evmgr

class TcpClientHandler(EventHandler):
    def __init__(self, bufsz, sock, addr, *args):
        self.__cln_addr = addr
        self.__buf_sz = bufsz
        super(TcpClientHandler, self).__init__(sock)
        self._fd.setblocking(False)
        # TODO: implement authentication (and probably authorization)
        em = evmgr()
        em.reg_in(self)
    def _impl(self, evt):
        em = evmgr()
        print('TcpClientHandler::_impl({})'.format(evt))
        # TODO: react to all events, not only input data...
        if evt == events.IN:
            self.__read()
            return
        if evt == events.OUT:
            self.__write()
            return
        if evt == events.CLO or evt == events.HUP or evt == events.RDH:
            print('HUP event')
            em.fire(self)
            return
        if evt == events.ERR:
            print('Error event catched')
            em.fire(self)
            return
    def _fini(self):
        try:
            # self._fd.close()
            # check if we are connected and shutdown the connection proper way
            print('TcpClientHandler::_fini(): perform specific operations')
        except:
            pass
    def __read(self):
        em = evmgr()
        try:
            dat = self._fd.recv(self.__buf_sz)
            if len(dat) == 0:
                raise Exception('No data read')
            em.unreg(self)
            em.reg_out(self)
        except Exception as e:
            print('!!! <= Exception: {}'.format(e))
            em.fire(self)
    def __write(self):
        em = evmgr()
        try:
            sz = self._fd.send(b'Answer')
            print('{} bytes sent'.format(sz))
            em.unreg(self)
            em.reg_in(self)
        except Exception as e:
            print('!!! => Exception: {}'.format(e))
            em.fire(self)

class TcpServer:
    class ConnectionAcceptHandler(EventHandler):
        def __init__(self, cfg, sock):
            self.__cfg = cfg
            self.__clients = list()
            super(TcpServer.ConnectionAcceptHandler, self).__init__(sock)
        def _impl(self, evt):
            print("TcpServer::ConnectionAcceptHandler::({})".format(evt))
            self.__clients.append(TcpClientHandler(self.__cfg['bufsz'], *self._fd.accept()))
        def _fini(self):
            try:
                print('TcpServer::ConnectionACceptHandler::_fini(): perform some specific operations')
            except:
                pass
    def __init__(self, buf, cfg):
        self.__buf = buf
        self.__cfg = cfg
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.__sock.bind((cfg['bind'], cfg['port']))
        self.__hnd = TcpServer.ConnectionAcceptHandler(self.__cfg, self.__sock)

    def start(self):
        em = evmgr()
        em.reg_in(self.__hnd)
        self.__sock.setblocking(False)
        self.__sock.listen(self.__cfg.get('maxconn', 10))

    def stop(self):
        em = evmgr()
        em.unreg(self.__hnd)
