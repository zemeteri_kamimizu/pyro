
class Task:
    def __init__(self):
        pass
    def run(self):
        self._impl()

class TaskHandler:
    def __init__(self):
        self.__tasks = []
    def add(self, task):
        self.__tasks.append(task)
    def run(self):
        for t in self.__tasks:
            t.run()
